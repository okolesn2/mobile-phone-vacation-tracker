package com.example.d308_vacationsandexcursionstracker.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.d308_vacationsandexcursionstracker.DAO.ExcursionDAO;
import com.example.d308_vacationsandexcursionstracker.DAO.VacationDAO;
import com.example.d308_vacationsandexcursionstracker.Entities.Excursion;
import com.example.d308_vacationsandexcursionstracker.Entities.Vacation;

@Database( entities = {Vacation.class, Excursion.class}, version =2 ,exportSchema = false)
public abstract class VacationDatabase extends RoomDatabase {
    public abstract VacationDAO vacationDAO();
    public abstract ExcursionDAO excursionDAO();
    private static volatile VacationDatabase INSTANCE;

    //instantiate an asynchronous Database
    static VacationDatabase getDatabase(final Context context){
       if (INSTANCE == null){
           synchronized (VacationDatabase.class){
               if(INSTANCE ==null){
                   INSTANCE= Room.databaseBuilder(context.getApplicationContext(),VacationDatabase.class, "MyVacationDatabase.db")
                           .fallbackToDestructiveMigration()
                           // To instantiate a synchronous database : add following code below
                           //.allowMainThreadQueries()
                           .build();
               }
           }
       }
       return INSTANCE;
    }


}
