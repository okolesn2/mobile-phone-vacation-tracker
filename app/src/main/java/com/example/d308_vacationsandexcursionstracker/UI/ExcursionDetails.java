package com.example.d308_vacationsandexcursionstracker.UI;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.d308_vacationsandexcursionstracker.Database.Repository;
import com.example.d308_vacationsandexcursionstracker.Entities.Excursion;
import com.example.d308_vacationsandexcursionstracker.Entities.Vacation;
import com.example.d308_vacationsandexcursionstracker.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class ExcursionDetails extends AppCompatActivity {

    Random rand = new Random();
    Repository repository;

    String title;
    String date;

    EditText editTitle;
    EditText editDate;

    int vacationID;
    int excursionID;

    Excursion currentExcursion;

    //ALLOWS FOR EDITING OF EXCURSION DETAILS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excursion_details);

        repository=new Repository(getApplication());

        //PART B.4: sets the excursion title
        editTitle = findViewById(R.id.titletext);
        title = getIntent().getStringExtra("title");
        editTitle.setText(title);

        //PART B.4: sets the excursion date
        editDate = findViewById(R.id.datetext);
        date = getIntent().getStringExtra("date");
        editDate.setText(date);

        excursionID = getIntent().getIntExtra("id", -1);
        vacationID = getIntent().getIntExtra("vacationID", -1);

    }

    //CREATES MENU ON EXCURSION DETAILS SCREEN
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_excursiondetails, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        boolean isStartDateValid= isValidDateFormat(editDate.getText().toString() );

        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        }

        //PART B.5.d : user can set an alert that will trigger on the excursion date
        if (item.getItemId() == R.id.notifyDate) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");

            //PART B.5.c :isStartDateValid returns true when the input dates are formatted correctly.
            if( isStartDateValid ) {
                String date_string = editDate.getText().toString();
                boolean isExcursionDateValid= isExcursionDateDuringVacation( date_string);

                //PART B.5.e :isExcursionDateValid returns true when excursion date is between the associated vacation startDate and endDate.
                if( isExcursionDateValid ) {
                    //Converts startDate from string to  a date object
                    Date date1 = null;
                    try {
                        date1 = sdf.parse(date);
                    } catch (ParseException e) {
                        Toast.makeText(ExcursionDetails.this, "Notification ParseException Error : date", Toast.LENGTH_LONG).show();
                        throw new RuntimeException(e);
                    }

                    try {
                        int rand_int1 = rand.nextInt(100000);

                        //Create start date trigger
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.HOUR, 00);
                        cal.set(Calendar.MINUTE, 00);
                        cal.set(Calendar.SECOND, 00);

                        assert date1 != null;
                        cal.setTime(date1);
                        long trigger = cal.getTimeInMillis();

                        //NOTIFICATION
                        Intent intent = new Intent(ExcursionDetails.this, MyReceiver.class);
                        intent.putExtra("key", "Your " + title + " excursion starts on " + date);

                        PendingIntent sender = PendingIntent.getBroadcast(ExcursionDetails.this, rand_int1, intent, PendingIntent.FLAG_IMMUTABLE);
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        alarmManager.set(AlarmManager.RTC_WAKEUP, trigger, sender);
                        Toast.makeText(ExcursionDetails.this, " Alarm set for: " + date + " at Mid-Night", Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ExcursionDetails.this, "Notification Exception Error 2: start date ", Toast.LENGTH_LONG).show();
                    }

                    return true;
                }
            }
        }

        //PART B.5.b : enables user to enter, edit, and delete excursion information
        if (item.getItemId() == R.id.excursionsave) {
            Excursion excursion;
            if (isStartDateValid  ) {
                String date_string = editDate.getText().toString();
                boolean isExcursionDateValid= isExcursionDateDuringVacation( date_string);
                if( isExcursionDateValid ) {
                    if (excursionID == -1) {
                        Log.d("SAVE EXCURSION ", "exursionID == -1");
                        if (repository.getAllExcursions().size() == 0) {
                            Log.d("SAVE EXCURSION 2", "repository.getAllExcursions().size() == 0 , set excursionID to 1");
                            excursionID = 1;
                        } else {
                            Log.d("SAVE EXCURSION 2 ", "repository.getAllExcursions().size() != 0");
                            excursionID = repository.getAllExcursions().get(repository.getAllExcursions().size() - 1).getExcursionID() + 1;
                            Toast.makeText(ExcursionDetails.this, "Excursion was saved!", Toast.LENGTH_LONG).show();
                        }

                        Log.d("SAVE EXCURSION ", "excursionID = 1 ");
                        excursion = new Excursion(excursionID, editTitle.getText().toString(), editDate.getText().toString(), vacationID);
                        repository.insert(excursion);
                        Log.d("SAVE EXCURSION ", "new excursion with ID of 1 is inserted into repository");
                        this.finish();

                    } else {
                        excursion = new Excursion(excursionID, editTitle.getText().toString(), editDate.getText().toString(), vacationID);
                        repository.update(excursion);
                        Toast.makeText(ExcursionDetails.this, "Excursion was saved", Toast.LENGTH_LONG).show();
                        this.finish();
                    }
                }
            }
        }

        //PART B.5.b : enables delete excursion information frm the database
        if (item.getItemId() == R.id.excursiondelete) {
            for (Excursion excur : repository.getAllExcursions()) {
                if (excur.getExcursionID() == excursionID) {
                    currentExcursion = excur;
                }
            }
            repository.delete(currentExcursion);
            Toast.makeText(ExcursionDetails.this, currentExcursion.getTitle() + " was deleted", Toast.LENGTH_LONG).show();
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    //PART B.5.c :isValidDateFormat function validates that the input dates are formatted correctly.
    public  boolean isValidDateFormat(String toValidate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        sdf.setLenient(false); // Disable lenient parsing

        try {
            sdf.parse(toValidate);
            return true;
        } catch (ParseException e) {
            Toast.makeText(ExcursionDetails.this, "Incorrect date format", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public  boolean isExcursionDateDuringVacation (String excursionDate) {
        Calendar min =null;
        Calendar max =null;
        Calendar target =null;

        String min_str =null;
        String max_str =null;

        for (Vacation vacation : repository.getmAllVacations()) {
            if (vacation.getVacationID() == vacationID)
            {
                 min= stringToCalendar(vacation.getStartDate());
                 max= stringToCalendar(vacation.getEndDate());
                 target= stringToCalendar(excursionDate);

                 min_str =vacation.getStartDate();
                 max_str =vacation.getEndDate();
            }
        }
        if(  target.after(min) && target.before(max)){
            return true;
        }
        Toast.makeText(ExcursionDetails.this, "Excursion date is not between " +min_str + " and "+max_str, Toast.LENGTH_LONG).show();
        return false;
    }

    //stringToCalendar converts a string to a calander object
    public Calendar stringToCalendar (String strDate){
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
        Date date = null;
        try {
            date = formatter.parse(strDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        Calendar cal = Calendar.getInstance();
        assert date != null;
        cal.setTime(date);
        return cal;
    }
}







