package com.example.d308_vacationsandexcursionstracker.UI;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.d308_vacationsandexcursionstracker.Database.Repository;
import com.example.d308_vacationsandexcursionstracker.Entities.Excursion;
import com.example.d308_vacationsandexcursionstracker.Entities.Vacation;
import com.example.d308_vacationsandexcursionstracker.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Arrays;
import java.util.List;
  public class VacationList extends AppCompatActivity {
      //Instantiate the respiratory
      private Repository repository;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacation_list);

        FloatingActionButton fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(VacationList.this, VacationDetails.class);
                startActivity(intent);
            }
        });
        RecyclerView recyclerView=findViewById(R.id.recyclerView);
        repository = new Repository(getApplication());
        List<Vacation> allVacations = repository.getmAllVacations();
        final VacationAdapter vacationAdapter = new VacationAdapter(this);
        recyclerView.setAdapter(vacationAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        vacationAdapter.setVacations(allVacations);
        //System.out.println(getIntent.getStringExtra("test"));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_vacation_list,menu);
        return true;
    }
      @Override
      protected void onResume() {

          super.onResume();
          List<Vacation> allVacations = repository.getmAllVacations();
          RecyclerView recyclerView = findViewById(R.id.recyclerView);
          final VacationAdapter vacationAdapter = new VacationAdapter(this);
          recyclerView.setAdapter(vacationAdapter);
          recyclerView.setLayoutManager(new LinearLayoutManager(this));
          vacationAdapter.setVacations(allVacations);

          //Toast.makeText(ProductDetails.this,"refresh list",Toast.LENGTH_LONG).show();
      }



    //Add to database
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.mysample){

            //Create instance of repository
            repository = new Repository(getApplication());

            //Toast.makeText(VacationList.this,"put in sample data",Toast.LENGTH_LONG).show();

            //create a vacation and insert to repository
            Vacation vacation= new Vacation(1,"1st Vacation", "Royal Hotel","1-1-2024","3-1-2024");
            repository.insert(vacation);

            vacation= new Vacation(2,"2nd Vacation", "Luxury Hotel","3-1-2024","6-1-2024");
            repository.insert(vacation);


            //create excursion associated with vacation and insert into repository
            Excursion excursion= new Excursion(1, "Snorkling","1-1-2024",1);
            repository.insert(excursion);

            excursion= new Excursion(2, "Hiking","3-1-2024",1);
            repository.insert(excursion);

            return true;
        }
        if(item.getItemId() == android.R.id.home){
            this.finish();

            //Intent intent = new Intent ( VacationList.this,VacationDetails.class);
            //startActivity(intent);
            return true;
        }
        if(item.getItemId() == R.id.shareVacations){

            String AllVacations = "";

            for (Vacation vac : repository.getmAllVacations()) {
                Log.d("CLIPBOARD ---------- ", vac.getTitle());
                Log.d("CLIPBOARD ---------- ", vac.getHotel());
                Log.d("CLIPBOARD ---------- ", vac.getStartDate());
                Log.d("CLIPBOARD ---------- ", vac.getEndDate());

                String oneVacation = " Title: " +vac.getTitle() + " Hotel: " +vac.getHotel() +" Start Date: "+ vac.getStartDate() +" End Date: "+ vac.getEndDate();

                 AllVacations= AllVacations + "\n" + oneVacation ;
            }

            Log.d("CLIPBOARD ---------- ", AllVacations);

            //Instantiate clipboard
            ClipboardManager myClipboard;
            myClipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);




            //copy data
            ClipData myClip;

            myClip = ClipData.newPlainText("text", AllVacations);
            myClipboard.setPrimaryClip(myClip);


        }

        return true;
      }

}