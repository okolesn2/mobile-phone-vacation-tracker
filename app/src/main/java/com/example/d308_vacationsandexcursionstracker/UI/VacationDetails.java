package com.example.d308_vacationsandexcursionstracker.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.d308_vacationsandexcursionstracker.Database.Repository;
import com.example.d308_vacationsandexcursionstracker.Entities.Excursion;
import com.example.d308_vacationsandexcursionstracker.Entities.Vacation;
import com.example.d308_vacationsandexcursionstracker.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class VacationDetails extends AppCompatActivity {
    Repository repository;

    Random rand = new Random();

    String title;
    String hotel;
    String startDate;
    String endDate;
    int vacationID;

    EditText editTitle;
    EditText editHotel;
    EditText editStartDate;
    EditText editEndDate;

    Vacation currentVacation;
    int numExcursions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacation_details);

        //Sets the 'titletext' on the vacation detail screen
        editTitle = findViewById(R.id.titletext);
        title = getIntent().getStringExtra("title");
        editTitle.setText(title);

        //Sets the 'hoteltext' on the vacation detail screen
        editHotel = findViewById(R.id.hoteltext);
        hotel = getIntent().getStringExtra("hotel");
        editHotel.setText(hotel);

        //Sets the 'startdatetext' on the vacation detail screen
        editStartDate = findViewById(R.id.startdatetext);
        startDate = getIntent().getStringExtra("startDate");
        editStartDate.setText(startDate);

        //Sets the 'enddatetext' on the vacation detail screen
        editEndDate = findViewById(R.id.enddatetext);
        endDate = getIntent().getStringExtra("endDate");
        editEndDate.setText(endDate);

        //initializes date picker
        MaterialButton button = findViewById(R.id.rangePicker);
        TextView startingDate = findViewById(R.id.startdatetext);
        TextView endingDate = findViewById(R.id.enddatetext);

        //Displays the Date Range Picker and allows user to sets start and end date
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialDatePicker<Pair<Long, Long>> materialDatePicker = MaterialDatePicker.Builder.dateRangePicker().setSelection(new Pair<>(
                        MaterialDatePicker.thisMonthInUtcMilliseconds(),
                        MaterialDatePicker.todayInUtcMilliseconds()
                )).build();
                materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Pair<Long, Long>>() {
                    @Override
                    public void onPositiveButtonClick(Pair<Long, Long> selection) {
                        String date1 = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault()).format(new Date(selection.first));
                        String date2 = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault()).format(new Date(selection.second));

                        startingDate.setText(MessageFormat.format("{0}", date1));
                        endingDate.setText(MessageFormat.format("{0}", date2));
                    }
                });

                materialDatePicker.show(getSupportFragmentManager(), "tag");


            }
        });


        vacationID= getIntent().getIntExtra("id", -1);

        RecyclerView recyclerView = findViewById(R.id.excursionrecyclerview );
        repository = new Repository(getApplication());
        final ExcursionAdapter excursionAdapter = new ExcursionAdapter (this);
        recyclerView.setAdapter(excursionAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<Excursion> filteredExcursions = new ArrayList<>();

        for(Excursion p : repository.getAllExcursions()){
            if (p.getVacationID() == vacationID)
                filteredExcursions.add(p);
        }
        excursionAdapter.setExcursions(filteredExcursions);


        FloatingActionButton fab = findViewById(R.id.floatingActionButton2);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(VacationDetails.this, ExcursionDetails.class);
                intent.putExtra("vacationID", vacationID);
                startActivity(intent);
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vacationdetails, menu);
        return true;
    }
    public  boolean isValidDateFormat(String toValidate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        sdf.setLenient(false); // Disable lenient parsing

        try {
            sdf.parse(toValidate);
            return true;
        } catch (ParseException e) {
            Toast.makeText(VacationDetails.this, "Error: Date is not in MM-dd-yyyy format ", Toast.LENGTH_LONG).show();
            return false;
        }
    }
    public  boolean isEndDateAfterStartDate(String endDate, String startDate) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");

        LocalDate firstDate = LocalDate.parse (endDate,formatter);
        LocalDate secondDate = LocalDate.parse (startDate,formatter);

        if(firstDate.isAfter(secondDate)){
            return true;
        }
        else{
            Toast.makeText(VacationDetails.this, "Start Date cannot be after the End Date", Toast.LENGTH_LONG).show();
            return false;
        }
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean isStartDateValid= isValidDateFormat(editStartDate.getText().toString() );
        boolean isEndDateValid =isValidDateFormat( editEndDate.getText().toString());

        if(item.getItemId()== android.R.id.home){
            this.finish();
            return true;
        }
        // DELETE VACATIONS MENU BUTTON WITH VALIDATION
        if (item.getItemId() == R.id.vacationdelete) {
            for (Vacation vac : repository.getmAllVacations()) {
                if (vac.getVacationID() == vacationID) currentVacation = vac;
            }
            numExcursions = 0;
            for (Excursion excursion : repository.getAllExcursions()) {
                if (excursion.getVacationID() == vacationID) ++numExcursions;
            }

            if (numExcursions == 0) {
                repository.delete(currentVacation);
                this.finish();
                Toast.makeText(VacationDetails.this, currentVacation.getTitle() + " was deleted", Toast.LENGTH_LONG).show();
            }

            //PART B.1.a : Vacations cannot be deleted if excursions are associated with it
            else {
                Toast.makeText(VacationDetails.this, "Can't delete a vacation with excursions", Toast.LENGTH_LONG).show();
            }
            return true;
        }
        //VALIDATES THAT THE DATE IS FORMATTED CORRECTLY BEFORE SAVING
        if( isStartDateValid && isEndDateValid) {
            //VALIDATES THAT THE END DATE IS AFTER TEH START DATE
            if(isEndDateAfterStartDate(editEndDate.getText().toString(),editStartDate.getText().toString() )){
            //SAVE VACATIONS MENU BUTTON
                if (item.getItemId() == R.id.vacationsave) {
                    Vacation vacation;

                    if (vacationID == -1) {
                        if (repository.getmAllVacations().size() == 0) {
                            vacationID = 1;
                        }
                        else {
                            vacationID = repository.getmAllVacations().get(repository.getmAllVacations().size() - 1).getVacationID() + 1;
                        }

                        vacation = new Vacation(vacationID, editTitle.getText().toString(), editHotel.getText().toString(), editStartDate.getText().toString(), editEndDate.getText().toString());
                        repository.insert(vacation);
                        Toast.makeText(VacationDetails.this, "Vacation Saved", Toast.LENGTH_LONG).show();
                    }
                    else {
                        try {
                            vacation = new Vacation(vacationID, editTitle.getText().toString(), editHotel.getText().toString(), editStartDate.getText().toString(), editEndDate.getText().toString());
                            repository.update(vacation);
                            Toast.makeText(VacationDetails.this, "Vacation Saved", Toast.LENGTH_LONG).show();
                        }
                        catch (Exception e) {}
                    }
                    return true;
                }

                //Notifications must be turned on by the user manually
                if(item.getItemId()== R.id.notifyStart) {
                    Log.d("NOTIFICATION ", "notifyStart menu button triggered");
                    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");

                    //Converts startDate from string to  a date object
                    Date date = null;
                    try {
                        date = sdf.parse(startDate);
                    }
                    catch (ParseException e) {
                        Toast.makeText(VacationDetails.this, "Notification ParseException Error : date_start", Toast.LENGTH_LONG).show();
                        throw new RuntimeException(e);
                    }

                    try{

                        int rand_int1 = rand.nextInt(100000);
                        Log.d("NOTIFICATION ", String.valueOf(rand_int1));

                        //Create start date trigger
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.HOUR, 00);
                        cal.set(Calendar.MINUTE, 00);
                        cal.set(Calendar.SECOND, 00);

                        assert date != null;
                        cal.setTime(date);
                        long trigger = cal.getTimeInMillis();

                        //NOTIFICATION FOR START DATE
                        Intent intent = new Intent(VacationDetails.this, MyReceiver.class);
                        intent.putExtra("key", "Your "+title+ " vacation starts on "+startDate );

                        PendingIntent sender = PendingIntent.getBroadcast(VacationDetails.this, rand_int1, intent, PendingIntent.FLAG_IMMUTABLE);
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        alarmManager.set(AlarmManager.RTC_WAKEUP, trigger, sender);
                        Toast.makeText(VacationDetails.this, " Alarm set for: "+ startDate +" at Mid-Night", Toast.LENGTH_LONG).show();
                    }
                    catch (Exception e){
                        Toast.makeText(VacationDetails.this, "Notification Exception Error 2: start date ", Toast.LENGTH_LONG).show();
                    }

                    return true;
                }


                if(item.getItemId()== R.id.notifyEnd) {

                    Log.d("NOTIFICATION ", "notifyEnd menu button triggered");
                    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");

                    //Converts endDate from string to a date object
                    Date date2 = null;
                    try {
                        date2 = sdf.parse(endDate);
                    }
                    catch (ParseException e) {
                        Toast.makeText(VacationDetails.this, "Notification ParseException Error : date_end ", Toast.LENGTH_LONG).show();
                        throw new RuntimeException(e);
                    }

                    try{
                        int rand_int2 = rand.nextInt(100000);
                        Log.d("NOTIFICATION ", String.valueOf(rand_int2));

                        //Create end date trigger
                        Calendar cal2 = Calendar.getInstance();
                        cal2.set(Calendar.HOUR, 00);
                        cal2.set(Calendar.MINUTE, 00);
                        cal2.set(Calendar.SECOND, 00);


                        assert date2 != null;
                        cal2.setTime(date2);
                        long trigger2 = cal2.getTimeInMillis();

                        //NOTIFICATION FOR END DATE
                        Intent intent2 = new Intent(VacationDetails.this, MyReceiver.class);
                        intent2.putExtra("key", "Your "+title+ " vacation ends on "+endDate );


                        PendingIntent sender2 = PendingIntent.getBroadcast(VacationDetails.this, rand_int2, intent2, PendingIntent.FLAG_IMMUTABLE);
                        AlarmManager alarmManager2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        alarmManager2.set(AlarmManager.RTC_WAKEUP, trigger2, sender2);
                        Toast.makeText(VacationDetails.this, " Alarm Set for: "+ endDate +" at Mid-Night", Toast.LENGTH_LONG).show();
                    }
                    catch (Exception e){
                        Toast.makeText(VacationDetails.this, "Notification Exception Error 2 : end date ", Toast.LENGTH_LONG).show();
                    }
                    return true;


                }


            }
        }
        return super.onOptionsItemSelected(item);
    }
}











