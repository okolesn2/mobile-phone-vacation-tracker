Title: Vacation Tracker

Purpose of the application: To help travelers track their vacations and excursions.


Directions for how to operate the application:
After opening the app, click "Start" from the home screen.
Click the "+" button on the bottom right corner to add a new vacation in the Vacation List screen.
Click the "Set Date" button to open the Android date picker in the Vacation Detail screen.
Click the "+" button on the bottom right corner to add a new excursion in the Vacation Details screen.
In the Excursion Details screen, click the menu icon on the top right corner to save and delete an excursion or set an alarm.
To share vacation details, navigate to the Vacation List screen, click the menu at the top right corner, and click "Share All Vacations".
Next, open the messenger app and select the clipboard icon to copy and paste all the vacation details into a text message.


Android version the signed APK is deployed to:
The minSdkVersion is 26, corresponding to Android version 8.0.0 version, named Oreo

Link to the git repository:
https://gitlab.com/wgu-gitlab-environment/student-repos/okolesn2/d308-mobile-application-development-android.git